# ProcessDefinitionKiller

## Aufgabenstellung

Es soll (bspw. mit Python/PowerShell) ein Skript erstellt werden, über welches die Prozessdefinitionen aus dem System gelöscht werden.

## Angaben zur Lösung

Das Skript benötigt keinen Parameter.
Zuerst wird eine Liste der vorhandenen Prozessdefinitionen angefordert. Danach werden diese einzeln gelöscht.
