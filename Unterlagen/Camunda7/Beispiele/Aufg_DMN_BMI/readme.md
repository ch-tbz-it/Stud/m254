# Geschäftsregeln mit DMN implementieren

## Aufgabenstellung

In einem Eingabeformular können Angaben über eine Person erfasst werden: Vorname, Gewicht und Grösse. Der "Body Mass Index" (BMI) wird vom System berechnet und auf einer weiteren Maske ausgegeben.

## Angaben zur Lösung

1) Deployen Sie die [BPMN-Datei](Aufg_BMI.bpmn) nach Camunda. Geben Sie dabei die drei anderen Dateien ([BMIberechnen.dmn](BMIberechnen.dmn), [Form_BMIAngabenErfassen.form](./Form_BMIAngabenErfassen.form) und [Form_BMIAusgeben.form](./Form_BMIAusgeben.form)) als "Additional Files" mit.
2) Starten Sie den Prozess "BMI berechnen mit DMN"
3) Füllen Sie das User-Formular aus (erst nach Refresh sichtbar)
