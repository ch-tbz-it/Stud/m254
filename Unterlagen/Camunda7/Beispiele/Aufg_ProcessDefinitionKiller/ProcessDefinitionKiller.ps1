# Achtung: Löscht ALLE Prozess-Definitionen
Read-Host -Prompt 

$engineEndpoint = "http://localhost:8080/engine-rest"
$UrlGetList = "$engineEndpoint/process-definition"
$UrlDeleteDefinition = "$engineEndpoint/process-definition/key/{0}?cascade=true"

# Liste der vorhandenen Definitionen anfordern
$result = Invoke-WebRequest -Uri $UrlGetList -Method Get
$defInstances = $result | ConvertFrom-Json

# alle gefundenen Definitionen löschen
foreach ($defInstance in $defInstances)
{
    Write-Host $defInstance.tenantId
    if ($null -eq $defInstance.tenantId){
        $UrlDeleteDefinitionCommand = $UrlDeleteDefinition -f $defInstance.key
    }
    else{
        $UrlDeleteDefinitionCommand = $UrlDeleteDefinition -f ($defInstance.key+"/tenant-id/"+$defInstance.tenantId+"/")
    }
    Write-Host ("Gelöscht wird Definition {0}" -f $defInstance.key) -ForegroundColor Yellow

    $resDelete = Invoke-WebRequest -uri $UrlDeleteDefinitionCommand -Method Delete

    if ($resDelete.StatusCode -lt 299) {
        Write-Host -ForegroundColor Green "Definition erfolgreich gelöscht mit Resultat " $resDelete.StatusCode $resDelete.StatusDescription
    } else {
        Write-Host -BackgroundColor Red "Löschung ergab Fehler: " $resDelete.StatusCode $resDelete.StatusDescription
    }
}