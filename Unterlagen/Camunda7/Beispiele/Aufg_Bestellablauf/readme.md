
## Wichtig: Topic "SendMail"
Im BPMN wird ein Externer Task mit Topic "SendMail" verwendet. Das dazugehörige Programm [MailSenden.py](../Aufg_MailSenden/MailSenden.py) muss vorgängig gestartet werden
