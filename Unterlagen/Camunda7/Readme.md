# Camunda Version 7


## Installation

**Voraussetzung**: Docker (oder Docker Desktop) ist installiert.

Für die Installation gehen Sie wie folgt vor:

1) Download des Docker-Image

``` cmd
docker pull camunda/camunda-bpm-platform:run-latest
```

2) Starten eines Docker-Containers

``` cmd
docker run -d --name camundaV7 -p 8080:8080 camunda/camunda-bpm-platform:run-latest
```

3) Browser öffnen mit der Url [http://localhost:8080](http://localhost:8080) (es dauert evtl. etwas, bis die Camunda-Umgebung gestartet ist).
  
4) Login mit User "demo" und Passwort "demo"

Alternativ kann die Version von Frau Bucher, WUP23a verwendet werden. Diese enthält zusätzliche Funktionen. So können aus dem Cockpit heraus Prozesse gestartet und gelöscht werden. Details und Installation siehe auf [dockerhub](https://hub.docker.com/r/mavbch/camunda254)

Weitere Möglichkeiten siehe [hier](https://docs.camunda.org/get-started/quick-start/install/)

## REST-API

Die Camunda-Engine bietet ein REST-API an. Mittels REST-Aufrufen kann Camunda nach Informationen über Prozesse, Tasks, etc. gefragt werden.

### Aufrufe mit Swagger-UI
REST-Aufrufe können mit Swagger sehr einfach und ohne Programmierung durchgeführt werden. Dazu ist aber etwas Vorarbeit nötig:

1) Eruieren der "Camunda BPM Runtime"-Version: Aufruf der [Diagnostics](http://localhost:8080/camunda/app/admin/default/#/system?section=diagnostics-system-settings)-Seite. Auslesen der "Product Version".
2) Downloaden des zugehörigen JAR-Files von der [Camunda-Engine REST OpenAPI](https://artifacts.camunda.com/ui/native/camunda-bpm/org/camunda/bpm/camunda-engine-rest-openapi/)-Webpage
3) Extrahieren der Openapi.json-Datei aus der JAR-Datei (Ändern der Endung *.JAR in *.ZIP)
4) Aufruf des [Swagger-Editors](https://editor-next.swagger.io/) und importieren der JSON-Datei.

Siehe auch Anmerkungen zu [OpenAPI](https://docs.camunda.org/manual/latest/reference/rest/openapi/) auf der Camunda-Website.

## Prozess-Beispiele

Im Ordner [Beispiele](./Beispiele/) finden Sie diverse kleine Prozesse, welche die Funktionsweise von BPMN-Symbolen zeigen.

Ausserdem bietet Camunda auf [GitHub](https://github.com/camunda/camunda-bpm-examples) eine grosse Anzahl von Beispielen an.
