# processDefinitionKey muss auf den Key der zu startenden Prozessdefinition gesetzt werden
$processDefinitionKey = "StartWithPowershell"

$headers = @{
    'accept' = 'application/json'
    'Content-Type' = 'application/json'
}

Invoke-RestMethod -Method Post -Uri "http://localhost:8080/engine-rest/process-definition/key/$processDefinitionKey/start" -Headers $headers