Externen Task einbinden
-------------------------

## Aufgabenstellung

Was bei einem bestimmten Task zu tun ist, soll in einem Python-Skript definiert werden. Anstelle dass also Camunda die Abarbeitung eines Tasks übernimmt, soll ein Python-Skript die Verarbeitung übernehmen.

## Angaben zur Lösung

Voraussetzung:
- Python muss installiert sein
- "camunda-external-task-client-python3" muss installiert sein
(```pip install camunda-external-task-client-python3```)

Vorgehen:
1) BPMN deployen
2) Python-Skript starten
3) Prozess starten. Das Ergebnis ist abhängig von einer Zufallszahl. Alternativ kann eine Variable "vorgabe" gesetzt werden.


### Signal für Abbruch des Hochzählens

curl -X "POST" "http://localhost:8080/engine-rest/signal" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"name\": \"StopCountingSignal\" } "

curl -X 'POST' 'http://localhost:8080/engine-rest/signal' -H 'accept: */*' -H 'Content-Type: application/json' -d '{ "name": "StopCountingSignal", "variables": { "neueVorgabe": { "value": 1 }, "Abbruchmeldung": { "value": "Hochzaehlen wurde durch Signal abgebrochen" } } '
