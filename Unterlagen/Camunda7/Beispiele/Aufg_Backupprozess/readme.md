# Backup-Prozess

## Besonderheiten

In diesem Beispiel sind folgende Spezialitäten zu finden:

- Call Activity
- Verarbeiten von Fehlern oder Escalation-Events über angeheftete (attached) Events

## Aufgabenstellung

Es soll ein Backupprozess definiert werden. Dieser startet, wenn ein Backup fällig wird (bestimmte Bedingungen treffen ein). Zuerst wird geprüft, ob freier Speicher verfügbar ist. Ist das der Fall, wird das Backup durchgeführt und der Prozess beendet.

Falls nicht genügend Speicher vorhanden ist, wird ein globaler Prozess "Speicherort auswählen" aufgerufen (Call Activity) und bei dessen Beendigung ebenfalls das Backup durchgeführt.

Wird im aufgerufenen Prozess ein Fehler ausgegeben, soll der Backupprozess mit einer Meldung abgebrochen werden. Wird eine Escalation ausgegeben, bedeutet dies, dass ein alternativer Speicherort gefunden wurde. In diesem Falle wird die Angabe des Speicherorts für den nächsten Backupprozess gespeichert. Das Backup wird normal durchgeführt.

## Angaben zur Lösung

Damit das Beispiel funktioniert, müssen alle Dateien (ausser dieser readme.md-Datei) deployed werden. Der Prozess "Backup Prozess" kann danach in der Tasklist gestartet werden.

Der Ablauf sollte aus dem BPMN ersichtlich sein. Die Skript-Tasks schreiben die Ausgabe ins Log.
