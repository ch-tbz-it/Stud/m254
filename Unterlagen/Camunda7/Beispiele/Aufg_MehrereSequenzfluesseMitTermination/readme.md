# Mehrere Sequenzflüsse aus einem Task mit Terminierung

## Besonderheiten

- Termination-Schlussevent
- Mehrere Sequenzflüsse aus einem Task heraus

## Angaben zur Lösung

Der Prozess erzeugt einen UserTask, welcher dem User "demo" zugeordnet ist.
Wird dieser abgeschlossen, erscheinen danach drei neue Task in der Liste (B1, B2 und B3_Terminate). Wird B3_Terminate abgeschlossen, verschwinden die anderen Tasks (B1, B2) auch, falls diese noch nicht abgeschlossen sind.

Der Grund: Der Termination-Schlussevent nach Task *B3_Terminate* vernichtet alle noch offenen Token.
