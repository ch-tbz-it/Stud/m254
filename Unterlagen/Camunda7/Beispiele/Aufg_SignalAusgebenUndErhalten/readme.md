# Signal ausgeben und empfangen

## Aufgabenstellung

Das Auslösen eines Signal-Events und die Reaktion darauf soll anhand eines Beispiels gezeigt werden.

## Angaben zur Lösung

Im BPMN-File [Aufg_Signale_div.bpmn](Aufg_Signale_div.bpmn) sind **drei** Prozesse definiert. Der erste und letzte Prozess kann ausgeführt werden. Der zweite Prozess startet als Reaktion auf einen Signaleingang.
Im File [Aufg_Signale_Teil2.bpmn](Aufg_Signale_Teil2.bpmn) ist ein weiterer Prozess definiert, welcher durch das gleiche Signal (EinSignal) gestartet wird.

### Ablauf/Vorgehen

1) Datei [Aufg_Signale_div.bpmn](Aufg_Signale_div.bpmn) deployen
2) Prozess "Signal-Test: Prozess mit empfang. Signal-Zwischenereignis" aufrufen und ersten "UserTask" completen. Der Prozess wartet danach auf ein Signal und ist daher in der Tasklist nicht mehr sichtbar.
3) Prozess "Signal-Test: Signal ausgeben" aufrufen und ersten "UserTask" completen. Der Prozess gibt darauf ein Signal aus.
4) Es sollten nun zwei UserTasks aus zwei Prozessinstanzen in der Tasklist sichtbar sein:
a) Der zweite Prozess wurde durch das Signal gestartet und steht beim UserTask "Signal erhalten".
b) Der dritte Prozess hat das Signal erhalten und steht nun beim UserTask "KontrollTask".
5) Die beiden Tasks können completed werden.
6) Die Datei [Aufg_Signale_Teil2.bpmn](Aufg_Signale_Teil2.bpmn) zusätzlich deployen.
7) Beim Starten des Signals mit dem Prozess "Signal-Test: Signal ausgeben" werden nun **zwei** Prozesse gestartet. Es sind danach zwei UserTask aus zwei unterschiedlichen Prozessen sichtbar.
