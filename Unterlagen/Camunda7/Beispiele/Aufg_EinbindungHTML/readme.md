# Einbindung von HTML-Formularen

## Aufgabenstellung

Auf einer HTML-Seite sollen verschiedene Eingabefelder zur Verfügung stehen (Vorname, Nachname, Alter). 
Über einen Button "Starten" wird ein Prozess gestartet.

## Angaben zur Lösung

1) Deployen Sie die [BPMN-Datei](VersuchMitHTML.bpmn) nach Camunda. Geben Sie dabei die zwei anderen Dateien ([test.html](test.html) und [AusgabeAbHTMLForm.form](AusgabeAbHTMLForm.form)) als "Additional Files" mit.
2) Öffnen Sie die Datei "test.html" (bspw. im Chrome-Browser).
3) Füllen Sie die Formularfelder aus und drücken Sie auf den Button. Dabei wird in Camunda mit den Angaben aus dem HTML-Formular ein Prozess gestartet.
4) Überzeugen Sie sich, dass in Camunda ein Prozess gestartet wurde. Arbeiten Sie den Prozess ab.
5) Der letzte Task (Aufgabentyp: Skript-Task) gibt einen Hinweis in der Konsole aus. Schauen Sie dort nach, ob dieser ausgegeben wurde.
